-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2023 at 01:33 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stl119dp`
--

-- --------------------------------------------------------

--
-- Table structure for table `findings`
--

CREATE TABLE `findings` (
  `id` int(11) NOT NULL,
  `prasarana` varchar(255) NOT NULL,
  `nomer_prasarana` varchar(255) NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `tanggal_input` date DEFAULT NULL,
  `keterangan` text NOT NULL,
  `foto` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `tanggal_selesai` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `findings`
--

INSERT INTO `findings` (`id`, `prasarana`, `nomer_prasarana`, `lokasi`, `tanggal_input`, `keterangan`, `foto`, `status`, `tanggal_selesai`) VALUES
(1, 'Wesel', 'w123', 'Depok baru', '2023-02-18', 'lidah kanan gantung', 'wesel.jpg', 'open', NULL),
(2, 'track circuit', '1223', 'emplasemen depok utara', '2023-02-21', '', 'bukti.jpg', 'open', NULL),
(3, 'track circuit', '3421', 'dipo depok utara', '2023-02-21', 'mati', 'mati.jpg', 'open', NULL),
(4, 'a', 'a', 'a', '2023-02-21', 'a', 'aa.jpg', 'open', NULL),
(5, 'sinyal', '23', 'emplasemen depok utara', '2023-02-21', 'mati', 'sinyal.jpg', 'open', NULL),
(6, 'a', 'aa', 'a', '2023-02-21', 'aaa', 'a.jpg', 'closed', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `findings`
--
ALTER TABLE `findings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `findings`
--
ALTER TABLE `findings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
