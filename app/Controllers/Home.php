<?php

namespace App\Controllers;

use CodeIgniter\I18n\Time;

use \App\Models\FindingsModel;


class Home extends BaseController
{
    protected $findingModel;

    public function __construct()
    {
        $this->findingModel = new FindingsModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Stl119dp | Welcome',
        ];
        return view('home/index', $data);
    }

    public function temuan()
    {
        $keyword = $this->request->getVar('keyword');

        if ($keyword) {
            $this->findingModel->search($keyword);
        } else {
            $this->findingModel;
        }

        $data = [
            'title' => 'Stl119dp | Dafter Temuan',
            'finding' => $this->findingModel->paginate(6, 'finding'),
            'pager' => $this->findingModel->pager
        ];

        return view('home/temuan', $data);
    }

    public function tambah()
    {
        $data = [
            'title' => 'Tambah Temuan',
            'validation' => \Config\Services::validation()
        ];

        return view('home/tambah', $data);
    }

    public function proses_tambah()
    {
        //validasi input
        if (!$this->validate(
            [
                'prasarana' => 'required',
                'nomer_prasarana' => 'required',
                'lokasi' => 'required',
                'keterangan' => 'required',
                'foto' => [
                    'rules' => 'max_size[foto,5000]|is_image[foto]|mime_in[foto,image/jpg,image/png,image/jpeg]',
                    'errors' => [
                        'max_size' => 'Ukuran Gambar Terlalu Besar',
                        'is_image' => 'File Yang Anda Pilih Bukan Gambar',
                        'mime_in' => 'File Yang Anda Pilih Bukan Gambar'
                    ]
                ]
            ],
        )) {
            $validation = \Config\Services::validation();
            return redirect()->to('/tambah')->withInput()->with('validation', $validation);
        }

        //ambil gambar 
        $file_foto = $this->request->getFile('foto');

        //apakah tidak ada gambar yang di upload
        if ($file_foto->getError() == 4) {
            $nama_foto = 'default.jpg';
        } else {
            //buat nama random untuk file yang di upload
            $nama_foto = $file_foto->getRandomName();

            //pindahkan file ke folder img
            $file_foto->move('img/upload', $nama_foto);
        }

        $this->findingModel->save([
            'prasarana' => $this->request->getVar('prasarana'),
            'nomer_prasarana' => $this->request->getVar('nomer_prasarana'),
            'lokasi' => $this->request->getVar('lokasi'),
            'tanggal_input' => Time::now('Asia/Jakarta', 'en_US'),
            'keterangan' => $this->request->getVar('keterangan'),
            'foto' => $nama_foto,
            'status' => $this->request->getVar('status'),
            'tanggal_selesai' => $this->request->getVar('tanggal_selesai')
        ]);

        session()->setFlashdata('pesan', 'Data Berhasil Ditambahkan');
        return redirect()->to('/temuan');
    }

    public function hapus($id)
    {

        //cari nama gambar berdasarkan id
        $finding = $this->findingModel->find($id);

        //cek jika gambar adalah default.jpg
        if ($finding['foto'] != 'default.jpg') {
            //hapus gambar di img/upload
            unlink('img/upload/' . $finding['foto']);
        }

        $this->findingModel->delete($id);
        session()->setFlashdata('pesan', 'Data Berhasil Dihapus');
        return redirect()->to('/temuan');
    }

    public function status($id)
    {
        $this->findingModel->save(
            [
                'id' => $id,
                'status' => $this->request->getVar('status')
            ]
        );

        session()->setFlashdata('pesan', 'Status Berhasil Diedit');
        return redirect()->to('/temuan');
    }

    public function status_close($id)
    {
        $this->findingModel->save(
            [
                'id' => $id,
                'status' => $this->request->getVar('status'),
                'tanggal_selesai' =>  Time::now('Asia/Jakarta', 'en_US')
            ]
        );

        session()->setFlashdata('pesan', 'Status Berhasil Diedit');
        return redirect()->to('/temuan');
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Edit Temuan',
            'validation' => \Config\Services::validation(),
            'finding' => $this->findingModel->find($id)
        ];

        return view('home/edit', $data);
    }

    public function proses_edit($id)
    {
        //validasi input
        if (!$this->validate(
            [
                'prasarana' => 'required',
                'nomer_prasarana' => 'required',
                'lokasi' => 'required',
                'tanggal_input' => 'required',
                'keterangan' => 'required',
                'foto' => [
                    'rules' => 'max_size[foto,5000]|is_image[foto]|mime_in[foto,image/jpg,image/png,image/jpeg]',
                    'errors' => [
                        'max_size' => 'Ukuran Gambar Terlalu Besar',
                        'is_image' => 'File Yang Anda Pilih Bukan Gambar',
                        'mime_in' => 'File Yang Anda Pilih Bukan Gambar'
                    ]
                ]
            ],
        )) {
            $validation = \Config\Services::validation();
            return redirect()->to('/edit')->withInput()->with('validation', $validation);
        }

        //ambil gambar
        $file_foto = $this->request->getFile('foto');

        //apakah tidak ada gambar yang di upload
        if ($file_foto->getError() == 4) {
            $nama_foto =  $this->request->getVar('fotoLama');
        } else {
            //buat nama random untuk file yang di upload
            $nama_foto = $file_foto->getRandomName();

            //pindahkan file ke folder img
            $file_foto->move('img/upload', $nama_foto);
        }

        $this->findingModel->save([
            'id' => $id,
            'prasarana' => $this->request->getVar('prasarana'),
            'nomer_prasarana' => $this->request->getVar('nomer_prasarana'),
            'lokasi' => $this->request->getVar('lokasi'),
            'tanggal_input' => $this->request->getVar('tanggal_input'),
            'keterangan' => $this->request->getVar('keterangan'),
            'foto' => $nama_foto,
            'status' => $this->request->getVar('status'),
            'tanggal_selesai' => $this->request->getVar('tanggal_selesai')
        ]);

        session()->setFlashdata('pesan', 'Data Berhasil Diedit');
        return redirect()->to('/temuan');
    }
}
