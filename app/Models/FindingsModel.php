<?php

namespace App\Models;

use CodeIgniter\Model;

class FindingsModel extends Model
{
    protected $table = 'findings';
    // protected $useTimestamps = 'true';
    protected $allowedFields = ['prasarana', 'nomer_prasarana', 'lokasi', 'tanggal_input', 'keterangan', 'foto', 'status', 'tanggal_selesai'];

    public function search($keyword)
    {
        return $this->table('findings')->like('prasarana', $keyword)->orLike('nomer_prasarana', $keyword)->orLike('lokasi', $keyword);
    }
}
