<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="container">
    <div class="row">
        <h2 class="pb-3">Form Edit</h2>

        <div class="col">
            <form action="/home/proses_edit/<?= $finding['id']; ?>" method="post" enctype="multipart/form-data">
                <?= csrf_field(); ?>
                <input type="hidden" name="fotoLama" value="<?= $finding['foto']; ?>">
                <div class="mb-3">
                    <label for="prasarana" class="form-label">Prasarana</label>
                    <input type="text" class="form-control text-capitalized" id="prasarana" name="prasarana" autofocus required value="<?= (old('prasarana')) ? old('prasarana') : $finding['prasarana'] ?>">
                </div>
                <div class="mb-3">
                    <label for="nomer_prasarana" class="form-label">Nomer Prasarana</label>
                    <input type="text" class="form-control text-capitalized" id="nomer_prasarana" name="nomer_prasarana" value="<?= (old('nomer_prasarana')) ? old('nomer_prasarana') : $finding['nomer_prasarana'] ?>" required>
                </div>
                <div class="mb-3">
                    <label for="lokasi" class="form-label">Lokasi</label>
                    <input type="text" class="form-control text-capitalized" id="lokasi" name="lokasi" value="<?= (old('lokasi')) ? old('lokasi') : $finding['lokasi'] ?>" required>
                </div>
                <div class="mb-3">
                    <label for="tanggal_input" class="form-label">Tanggal Input</label>
                    <input readonly type="date" class="form-control text-capitalized" id="tanggal_input" name="tanggal_input" required value="<?= (old('tanggal_input')) ? old('tanggal_input') : $finding['tanggal_input'] ?>">
                </div>
                <div class="mb-3">
                    <label for="keterangan" class="form-label">Keterangan</label>
                    <input type="text" class="form-control text-capitalized" id="keterangan" name="keterangan" required value="<?= (old('keterangan')) ? old('keterangan') : $finding['keterangan'] ?>">
                </div>
        </div>

        <div class="col">
            <div class="mb-3">
                <label for="foto" class="form-label">Foto</label><br />
                <img src="/img/upload/<?= (old('foto')) ? old('foto') : $finding['foto'] ?>" width="100px" class="mb-3" />
                <p><?= (old('foto')) ? old('foto') : $finding['foto'] ?></p>
                <input type="file" class="form-control text-capitalized" id="foto" name="foto" value="/img/upload/<?= (old('foto')) ? old('foto') : $finding['foto'] ?>">
            </div>


            <label for="status" class="form-label">Status</label>
            <input type="text" class="form-control text-capitalized mb-3" id="status" name="status" value="<?= (old('status')) ? old('status') : $finding['status'] ?>">
            <button type="submit" class="btn btn-primary mt-3 mb-5">Submit</button>
        </div>

        </form>
    </div>

</div>

<?= $this->endSection(); ?>