<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="container">
    <div class="row">
        <h2 class="pb-3">Form Tambah</h2>

        <div class="col">
            <form action="/home/proses_tambah" method="post" enctype="multipart/form-data">
                <?= csrf_field(); ?>
                <div class="mb-3">
                    <label for="prasarana" class="form-label">Prasarana</label>
                    <input type="text" class="form-control" id="prasarana" name="prasarana" autofocus required value="<?= (old('prasarana')); ?>">
                    <!-- <select class="form-select" aria-label="Default select example">
                        <option selected>Pilih Jenis Prasarana</option>
                        <option value="1">Catu Daya</option>
                        <option value="2">LCP</option>
                        <option value="3">Interlocking</option>
                        <option value="3">Panel Pelayanan</option>
                        <option value="3">Sinyal HUT/Cabin</option>
                        <option value="3">Baterai</option>
                        <option value="3">Genset</option>
                        <option value="3">Data Logger</option>
                        <option value="3">CTS</option>
                        <option value="3">Bangunan JPL</option>
                        <option value="3">JPL</option>
                        <option value="3">Pesawat Telpon</option>
                        <option value="3">Perekam Telpon</option>
                        <option value="3">Location Case</option>
                        <option value="3">Sinyal Blok</option>
                        <option value="3">Sinyal Masuk</option>
                        <option value="3">Sinyal Keluar</option>
                        <option value="3">Sinyal Langsir</option>
                        <option value="3">Sinyal Ulang</option>
                        <option value="3">CFT</option>
                        <option value="3">Desktop Tetra</option>
                        <option value="3">HT Sepura</option>
                        <option value="3">Telpon Ladang</option>
                        <option value="3">WS Digital</option>
                        <option value="3">WS</option>
                        <option value="3">Master Talkback</option>
                        <option value="3">Talkback</option>
                        <option value="3">Telpon</option>
                        <option value="3">Sentral CFT</option>
                        <option value="3">Modem CTS</option>
                        <option value="3">Base Station Digital</option>
                        <option value="3">RIG PPKA</option>
                        <option value="3">Base Station Analog</option>
                        <option value="3">Tower Radio</option>
                        <option value="3">Server Telpon</option>
                    </select> -->
                </div>
                <div class="mb-3">
                    <label for="nomer_prasarana" class="form-label">Nomer Prasarana</label>
                    <input type="text" class="form-control" id="nomer_prasarana" name="nomer_prasarana" required value="<?= (old('nomer_prasarana')); ?>">
                </div>
                <div class="mb-3">
                    <label for="lokasi" class="form-label">Lokasi</label>
                    <input type="text" class="form-control" id="lokasi" name="lokasi" required value="<?= (old('lokasi')); ?>">
                </div>
                <div class="mb-3">
                    <label for="keterangan" class="form-label">Keterangan</label>
                    <input type="text" class="form-control" id="keterangan" name="keterangan" required value="<?= (old('keterangan')); ?>">
                </div>
        </div>

        <div class="col">
            <div class="mb-3">
                <label for="foto" class="form-label">Foto</label>
                <div class="mb-3">
                    <input class="form-control" type="file" id="foto" name="foto">
                </div>
            </div>
            <div class="mb-3">
                <label for="status" class="form-label">status</label>
                <input type="text" class="form-control" id="status" name="status" value="open" readonly>
            </div>
            <button type="submit" class="btn btn-primary mt-3 mb-5">Submit</button>
        </div>
        </form>
    </div>

</div>

<?= $this->endSection(); ?>