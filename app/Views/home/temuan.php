<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="row">
    <div class="col-md-6">
        <form class="d-flex" role="search" action="" method="get">
            <input class="form-control me-3" type="text" placeholder="Cari data disini..." aria-label="Search" name="keyword">
            <button class="btn btn-outline-success" type="submit" name="submit">Search</button>
        </form>
    </div>
    <div class="col-md-6">
        <a href="/tambah" type="button" class="btn btn-outline-primary float-end mt-1">Tambah Daftar Temuan</a>
    </div>
</div>
<div class="row mt-5">
    <div class="col-md-12">
        <?php if (session()->getFlashdata('pesan')) : ?>
            <div class="alert alert-success" role="alert">
                <?= session()->get('pesan'); ?>
            </div>
        <?php endif; ?>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-hover align-middle table-borderless rounded">
                    <thead>
                        <tr>
                            <th scope="col">Prasarana</th>
                            <th scope="col">Nomer Prasarana</th>
                            <th scope="col">Lokasi</th>
                            <th scope="col">Tanggal Input</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">Foto</th>
                            <th scope="col">Status</th>
                            <th scope="col">Tanggal Selesai</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($finding as $f) : ?>

                            <tr>
                                <td><?= $f['prasarana'] ?></td>
                                <td><?= $f['nomer_prasarana'] ?></td>
                                <td><?= $f['lokasi'] ?></td>
                                <td><?= $f['tanggal_input'] ?></td>
                                <td><?= $f['keterangan'] ?></td>
                                <td><img src="/img/upload/<?= $f['foto']; ?>" alt="" style="width: 100px"></td>
                                <td>
                                    <div class="btn-group dropend">
                                        <?php if ($f['status'] == 'open') : ?>
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle text-capitalize" data-bs-toggle="dropdown" aria-expanded="false">
                                                <?= $f['status'] ?>
                                            </button>
                                        <?php elseif ($f['status'] == 'close') : ?>
                                            <button type="button" class="btn btn-danger btn-sm dropdown-toggle text-capitalize">
                                                <?= $f['status'] ?>
                                            </button>
                                        <?php else : ?>
                                            <button type="button" class="btn btn-warning btn-sm dropdown-toggle text-capitalize" data-bs-toggle="dropdown" aria-expanded="false">
                                                <?= $f['status'] ?>
                                            </button>
                                        <?php endif; ?>
                                        <ul class="dropdown-menu text-capitalize">
                                            <form action="/home/status/<?= $f['id']; ?>" method="post">
                                                <li>
                                                    <input onclick="return confirm('Siap untuk tindak lanjut?')" type="submit" class="dropdown-item text-capitalize <?= ($f['status'] == 'progress') ? 'disabled' : '' ?>" href="" id="status" name="status" value="progress">
                                                </li>
                                                <li>
                                            </form>
                                            <form action="/home/status_close/<?= $f['id']; ?>" method="post">
                                                <input onclick="return confirm('Yakin sudah di tindak lanjut?')" type="submit" class="dropdown-item text-capitalize <?= ($f['status'] == 'close') ? 'disabled' : '' ?>" href="" id="status" name="status" value="close">
                                                </li>
                                            </form>

                                        </ul>
                                    </div>

                                </td>
                                <td><?= $f['tanggal_selesai'] ?></td>
                                <td>
                                    <a type="button" href="/home/edit/<?= $f['id']; ?>" class="btn btn-warning">Edit</a>

                                    <form action="/home/<?= $f['id']; ?>" method="post" class="d-inline">
                                        <?= csrf_field(); ?>
                                        <input type="hidden" name="_method" id="" value="DELETE">
                                        <button type="submit" class="btn btn-danger" onclick="return confirm('Hapus?')">Hapus</button>
                                    </form>
                                </td>
                            </tr>


                        <?php endforeach; ?>


                    </tbody>
                </table>
                <?= $pager->links('finding', 'pagination'); ?>
            </div>
        </div>
    </div>
</div>



<?= $this->endSection('content'); ?>