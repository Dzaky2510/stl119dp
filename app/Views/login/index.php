<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="container ">
    <div class="container text-center bg-white rounded my-5">
        <div class="row">
            <div class="col-md-4">
                <img src="/img/akhlak.jpeg" class="img-fluid img-login" alt="...">
            </div>
            <div class="col-md-8 mt-5">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="NIPP">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Password</label>
                    <input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Password">
                </div>
                <a type="button" class="btn btn-success" href="/home">Login</a>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection('content'); ?>